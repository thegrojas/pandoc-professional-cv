---
documentclass: professional-cv
basics:
  name: John Doe
  label: Programmer
  picture: ''
  email: john@gmail.com
  phone: "(912) 555-4321"
  website: http://johndoe.com
  summary: A summary of John Doe...
  location:
    address: 2712 Broadway St
    postalCode: CA 94115
    city: San Francisco
    countryCode: US
    region: California
  profiles:
  - network: Twitter
    username: john
    url: http://twitter.com/john
work:
- company: ACME, Inc (Palo Alto, CA)
  position: Web Developer
  website: http://company.com
  startDate: '2010-10-01'
  endDate: ''
  summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus.
  highlights:
  - Donec et mollis dolor. Praesent et diam eget libero Adobe Coldfusion egestas mattis sit amet vitae augue.
  - Nam tincidunt congue enim, ut porta lorem Microsoft SQL lacinia consectetur.
  - Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  - Pellentesque auctor nisi id magna consequat JavaScript sagittis.
  - Aliquam at massa ipsum. Quisque bash bibendum purus convallis nulla ultrices ultricies.
- company: AJAX Hosting (Austin, TX)
  position: Lead Developer
  website: http://company.com
  startDate: '2009-01-01'
  endDate: '2010-01-01'
  summary: Aenean ut gravida lorem. Ut turpis felis, Perl pulvinar a semper sed, adipiscing id dolor.
  highlights:
  - Curabitur dapibus enim sit amet elit pharetra tincidunt website feugiat nisl imperdiet. Ut convallis AJAX libero in urna ultrices accumsan.
  - Cum sociis natoque penatibus et magnis dis MySQL parturient montes, nascetur ridiculus mus.
  - In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.
  - Nullam cursus suscipit nisi, et ultrices justo sodales nec. Fusce venenatis facilisis lectus ac semper.
volunteer:
- organization: Carboard City
  position: Volunteer
  website: http://ccb.com/
  startDate: '2012-01-01'
  endDate: '2013-01-01'
  summary: Vivamus PostgreSQL fermentum semper porta. Nunc diam velit PHP, adipiscing ut tristique vitae
  highlights:
  - Maecenas convallis ullamcorper ultricies stylesheets.
  - Quisque mi metus, unit tests CSS ornare sit amet fermentum et, tincidunt et orci.
  - Curabitur venenatis pulvinar tellus gravida ornare. Sed et erat faucibus nunc euismod ultricies ut id
education:
- institution: University of California, Berkeley
  area: Computer Science and Engineering
  studyType: Bachelor
  startDate: '2004-06-01'
  endDate: '2013-01-01'
  gpa: '4.0'
  courses:
  - DB1101 - Basic SQL
  - UX2093 - User Experience
  - OS3621 - Operative Systems
awards:
- title: Employee of the Month
  date: '2014-11-01'
  awarder: Company
  summary: There is no spoon.
publications:
- name: How to Write a Makefile
  publisher: Company
  releaseDate: '2014-10-01'
  website: http://publication.com
  summary: Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
skills:
- name: Web Development
  level: Master
  keywords:
  - HTML
  - CSS
  - Javascript
languages:
- language: English
  fluency: Native speaker
- language: French
  fluency: C1
interests:
- name: Wildlife
  keywords:
  - Ferrets
  - Unicorns
references:
- name: Jane Doe
  reference: The only home we've ever known a still more glorious dawn awaits two ghostly white figures in coveralls and helmets are soflty dancing trillion permanence of the stars consciousness.
...
