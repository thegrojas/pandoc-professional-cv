<h1 align="center">Medium Length Professional Curriculum Vitae - Pandoc Template</h1>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]() 
[![Version](https://img.shields.io/badge/version-0.0.1-informational)](./CHANGELOG.md) 
[![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](./LICENSE)

</div>

---

<p align="center"> Template and build system to create a Medium Length Professional CV using the JSON Resume Schema, Pandoc, LaTex and LuaTex.
    <br> 
</p>

<p align="center">
  <a href="#about">🧐 About</a> •
  <a href="#getting_started">🏁 Getting Started</a> •
  <a href="#usage">🤹 Usage</a> •
  <a href="#changelog">📜 Changelog</a> •
  <a href="#todo">✅ Todo</a> •
  <a href="#built_using">🔨 Built Using</a> •
  <a href="#contributing">⚙️ Contributing</a> •
  <a href="#credits">✍️ Credits</a> •
  <a href="#support">🤲 Support</a> •
  <a href="#license">⚖️ License</a>
</p>

![Screenshot](./screenshot.png)

## 🧐 About <a name="about"></a>

This is a template and build system to create a Medium Length Professional CV using the JSON Resume Schema, Pandoc, LaTex and LuaTex.

This is one of many templates in my [Pandoc Templates](#) collection.

## 🏁 Getting Started <a name="getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and local generation of documents.

### 🦺 Prerequisites

Before proceeding make sure you have the following tools installed and updated on your system:

- [Pandoc](https://pandoc.org/installing.html).
- [LaTex](https://www.latex-project.org/get/).
- [LuaTex](http://luatex.org/download.html), PdfLatex or XeLatex.
- [Make](https://www.gnu.org/software/make/)

**Debian/Ubuntu:**

```shell 
sudo apt install -y texlive-full pandoc make
```

### 📦 Installing

#### 1. Clone the repository

Open a terminal and clone this repository to your computer using git:

```sh
git clone https://gitlab.com/thegrojas/pandoc-professional-cv.git
```
Then `cd` into the cloned repo:

```sh
cd professional-cv
```

#### 2. Change the `Makefile` to suit your needs

I'm using LuaTex as my PDF engine for LaTex but you can use PdfLatex or XeLatex. Just change the value of `PDF_ENGINE` inside `Makefile`.

Also please note that I use a **non-standard** path for my `.cls` and `.sty` files. Please change the value of `TEXMFHOME` so that it installs those files on the correct folder on your system.

#### 3. Install using `make`

Just run the following command inside this repository to install all the `.cols` and `.latex` files:

```sh
make install
```

## 🤹 Usage <a name="usage"></a>

**Tip**: I set a help system on the `Makefile`. Just type `make` or `make help` and you will get a list of useful targets.

**Note**: This CV uses the [JSON Resume Schema](https://jsonresume.org/getting-started/), if you haven't already, please go ahead and get you resume/cv on a valid .json file.

Now, let's use the `sample.md` file as an example. Let's assume that I have a valid JSON resume somewhere in my computer and I converted it from JSON to YAML with a tool like [Json2Yaml](https://www.json2yaml.com/).

After converting to Yaml I will paste the contents into the front matter of `sample.md` and I will have a file that looks roughly like this (I will reduce it to this guide short):

```yaml

---
basics:
  name: John Doe
  label: Programmer
  picture: ''
  email: john@gmail.com
  phone: "(912) 555-4321"
  website: http://johndoe.com
  summary: A summary of John Doe...
  location:
    address: 2712 Broadway St
    postalCode: CA 94115
    city: San Francisco
    countryCode: US
    region: California
  profiles:
  - network: Twitter
    username: john
    url: http://twitter.com/john
work:
- company: Company
  position: President
  website: http://company.com
  startDate: '2013-01-01'
  endDate: '2014-01-01'
  summary: Description...
  highlights:
  - Started the company
volunteer:
- organization: Organization
  position: Volunteer
  website: http://organization.com/
  startDate: '2012-01-01'
  endDate: '2013-01-01'
  summary: Description...
  highlights:
  - Awarded 'Volunteer of the Month'
education:
- institution: University
  area: Software Development
  studyType: Bachelor
  startDate: '2011-01-01'
  endDate: '2013-01-01'
  gpa: '4.0'
  courses:
  - DB1101 - Basic SQL
awards:
- title: Award
  date: '2014-11-01'
  awarder: Company
  summary: There is no spoon.
publications:
- name: Publication
  publisher: Company
  releaseDate: '2014-10-01'
  website: http://publication.com
  summary: Description...
skills:
- name: Web Development
  level: Master
  keywords:
  - HTML
  - CSS
  - Javascript
languages:
- language: English
  fluency: Native speaker
interests:
- name: Wildlife
  keywords:
  - Ferrets
  - Unicorns
references:
- name: Jane Doe
  reference: Reference...
```

Then, I will save the contents of `sample.md`, proceed to open a terminal in the folder containing that file, and just then, build the final pdf:

```sh
make build sample.pdf
```

Now it's just a matter of opening the file with my favorite .pdf viewer.

## 📜 Changelog <a name="changelog"></a>

All notable changes to this project will be documented in `CHANGELOG.md`. Click [here](./CHANGELOG.md) to take a look.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## ✅ Todo <a name="todo"></a>

Pending tasks will be tracked in `TODO.md`. Click [here](./TODO.md) to take a look

## 🔨 Built Using <a name="built_using"></a>

- [Make](https://www.gnu.org/software/make/) - Build System
- [LaTex](https://www.latex-project.org/) - Typesetting System
- [LuaLatex](http://luatex.org/) - PDF Generation Engine
- [Pandoc](https://pandoc.org/index.html) - Document Conversion System

## ⚙️ Contributing <a name="contributing"></a>

Developers, Testers and Translators are all welcomed. For more information on how to contribute to this project please refer to `CONTRIBUTING.md`. Click [here](./CONTRIBUTING.md) to take a look.

## ✍️ Credits <a name="credits"></a>

- [JSON Resume Team](https://jsonresume.org/team/) for there work on the schema.
- [Trey Hunner](http://www.treyhunner.com/) is the original creator of this [Medium Length Professional Curriculum Vitae Template](https://www.latextemplates.com/template/medium-length-professional-cv) with some modifications by the [LaTexTemplates Team](https://www.latextemplates.com/).
- [@kylelobo](https://github.com/kylelobo) creator of [The Documentation Compendium](https://github.com/kylelobo/The-Documentation-Compendium) whose templates I used as inspiration for my README's.

See also the list of [contributors](#) who participated in this project.

## 🤲 Support <a name="support"></a>

For now, just by sharing and/or staring this repository you are supporting my work.

## ⚖️ License <a name="license"></a>

This code is released under the [GLPv3](./LICENSE) license. For more information refer to `LICENSE.md`
